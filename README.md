![assembly-logo](docs/a.png)

# Assembly

_Lerna multi-packages repository to handle [Material UI](https://material-ui-next.com/) components._

## LICENSE

[MIT](./LICENSE.md)
